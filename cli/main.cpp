#include <glib/poppler.h>
#include <cairo/cairo.h>
#include <cairo/cairo-pdf.h>
#include <string>
#include <iostream>
#include <cstring>
#include <cmath>
#include <vector>
#include <cstdlib>
#include <tclap/CmdLine.h>
#include <mzposterize.hpp>

// TODO
// offset x & y as parameter
// list images on page
// allow image selection via parameter


int main(int argc, char **argv) {
    int retval(NO_ERROR);
    std::string file("file://");
    std::string dstfile;
    double dest_width, dest_height;
    double margin;

    try {

	TCLAP::CmdLine cmd("Command description message", ' ', "0.1");
	TCLAP::ValueArg<std::string> nameArg("s", "size", "ISO destination size", false, "", "string");
	TCLAP::ValueArg<int> pageArg("p", "page", "Select page", true, 0, "integer");
	TCLAP::ValueArg<int> imageArg("i", "image", "Select image instead of whole page", false, -1,"integer");
	TCLAP::ValueArg<double> widthArg("w", "width", "Destination width in mm", false, 0.0, "double");
	TCLAP::ValueArg<double> heightArg("h", "height", "Destination height in mm", false, 0.0, "double");
	TCLAP::ValueArg<double> marginArg("m", "margin", "Margin in mm", false, 0.0, "double");
	TCLAP::UnlabeledValueArg<std::string> sourcefile("srcfile", "source PDF", true, "", "srcfile");
	TCLAP::UnlabeledValueArg<std::string> destfile("dstfile", "destination PDF", true, "", "destfile");
	cmd.add(nameArg);
	cmd.add(imageArg);
	cmd.add(widthArg);
	cmd.add(pageArg);
	cmd.add(marginArg);
	cmd.add(sourcefile);
	cmd.add(destfile);
	cmd.parse(argc, argv);

	file += sourcefile.getValue();
	dstfile = destfile.getValue();

	MZPosterize posterize(file, dstfile, 
			[](const std::string& txt) { std::cout << txt;} );
    if(nameArg.isSet())
        posterize.setOutputPagesize(nameArg.getValue());

	if(marginArg.isSet())
	    posterize.changeMarging(marginArg.getValue());
	if(pageArg.isSet())
	    posterize.selectPage(pageArg.getValue());
	if(widthArg.isSet())
	    posterize.setOutputPageWidth(widthArg.getValue());
	if(heightArg.isSet())
		posterize.setOutputPageHeight(heightArg.getValue());
	if(imageArg.isSet())
		posterize.selectImage(imageArg.getValue());
	
	posterize.showReferenceBox(true);
	posterize.convert();

    } catch(TCLAP::ArgException &e) {
	std::cerr << "error: " << e.error() << " " << e.argId() << "\n";
    }
    return 0;
}
