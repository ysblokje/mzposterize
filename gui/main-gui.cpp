#include <wx/wx.h>
#include <wx/app.h>
#include <wx/progdlg.h>
#include <wx/config.h>
#include <wx/thread.h>

#include <mzp_gui.h>
#include <PDFView.h>
#include <mzposterize.hpp>

#define ID_MYFRAME 1
#define ID_TIMER 2

class MyFrame;

wxDEFINE_EVENT(wxEVT_COMMAND_MYTHREAD_COMPLETED, wxThreadEvent);
wxDEFINE_EVENT(wxEVT_COMMAND_MYTHREAD_INFO, wxThreadEvent);
wxDEFINE_EVENT(wxEVT_COMMAND_LOG_TXT, wxThreadEvent);


class MyThread : public wxThread
{
public:
    MyThread(MyFrame *handler, MZPosterize &posterizer)
        : wxThread(wxTHREAD_DETACHED), posterizer(posterizer)
        { m_handler = handler; }
    ~MyThread() {}
protected:
    virtual ExitCode Entry(); 
	MZPosterize &posterizer;
    MyFrame *m_handler;
};


class MyFrame : public mzp_frame {
public :
	MyFrame(wxWindow* parent, wxWindowID id = ID_MYFRAME, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 500,300 ), long style = wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL ) :
		mzp_frame(parent, id, title, pos, size, style), width(-1.0), height(-1.0) {
			posterizer.setLog([this](const std::string &txt) {
				wxThreadEvent* t=new wxThreadEvent(wxEVT_THREAD, wxEVT_COMMAND_LOG_TXT);
				t->SetString(txt.c_str());
				wxQueueEvent(GetEventHandler(), t);
			}
			);
			sizes = MZPosterize::getSizes();
			FillSizes(sizes);
			wxConfig config("mzposterize", "Minze Zwerver", "mzposterize.conf" );
			wxString tmp;
			if(config.Read("lastlnputfile", &tmp)) {
				previewCheck(tmp);
				m_inputfile->SetPath(tmp);
			}
			if(config.Read("lastoutput", &tmp))
				m_outputfile->SetPath(tmp);
			bool border;
			if(config.Read("borders", &border))
				m_chkborders->SetValue(border);
			
			int selection=wxNOT_FOUND;
			if(config.Read("pagesize", &tmp))
				selection = m_sizechoice->FindString(tmp); 
			
			m_sizechoice->SetSelection(selection);
		}
	void FillSizes(const MZPosterize::PageSizes &sizes) {
		for(MZPosterize::PageSizes::const_iterator it(sizes.begin()); it!=sizes.end(); it++) {
			m_sizechoice->Append(it->name.c_str(), const_cast<MZPosterize::PageSize*>(&(*it)));
		}
	}
    void OnCloseWindow(wxCloseEvent& /*event*/) {
		wxConfig config("mzposterize", "Minze Zwerver", "mzposterize.conf" );
		config.Write("lastlnputfile", m_inputfile->GetPath());
		config.Write("lastoutput", m_outputfile->GetPath());
		config.Write("borders", m_chkborders->IsChecked());
		int selection = m_sizechoice->GetSelection();
		if(selection != wxNOT_FOUND)
			config.Write("pagesize", m_sizechoice->GetString(selection));
		Destroy();
	}
	void onCancel(wxCommandEvent& event) {
		Destroy();
	}
	
	void onApply(wxCommandEvent& event) {
		startConversion();
	}
	
	void startConversion() {
		int selection = m_sizechoice->GetSelection();
		if(selection != wxNOT_FOUND) {
			MZPosterize::PageSize* pagesize = reinterpret_cast<MZPosterize::PageSize*>(m_sizechoice->GetClientData(selection));
			posterizer.setOutputPageHeight(pagesize->height);
			posterizer.setOutputPageWidth(pagesize->width);
		}
		
		posterizer.setSource(("file://" + m_inputfile->GetPath()).fn_str().data());
		posterizer.setDestinationFile(m_outputfile->GetPath().fn_str().data());
		posterizer.showReferenceBox(m_chkborders->IsChecked());
		posterizer.selectPage(m_pagenumber->GetValue()-1);
		
		dialog = new wxProgressDialog(wxT("Generating output"), wxT("Preparing..."), (max_pages = posterizer.prepare()), this, wxPD_AUTO_HIDE | wxPD_APP_MODAL);
		counter = 0;
		MyThread *thread = new MyThread(this, posterizer);
		if ( thread->Run() != wxTHREAD_NO_ERROR )
		{
			wxLogError("Can't create the thread!");
			delete thread;
		}
	}
	void onThreadUpdate(wxThreadEvent &event) {
		if(dialog) {
			dialog->Update(counter++, wxString::Format(wxT("Creating page %1$d/%2$d"), event.GetInt()+1, max_pages));
		}
	}
	void onThreadCompletion(wxThreadEvent &event) {
		std::cerr <<  "thread complete event" << std::endl;
		dialog->Update(max_pages);
		delete dialog;
		dialog = NULL;
	}
	
	void onLog(wxThreadEvent &event) {
		m_txtlog->AppendText(event.GetString());
	}

	void previewCheck(const wxString &file) {
		if(wxFileName::FileExists(file))  {
			posterizer.setSource(("file://" + file).fn_str().data());
			m_inputview->setPage(posterizer.getPage(m_pagenumber->GetValue()-1));
			m_preview->Enable(true);
			m_inputview->Show(true);
		} else {
			m_preview->Enable(false);
			m_inputview->Show(false);
			m_inputview->setPage(NULL);
		}
		Layout();
	}

	void enablePreview(wxFileDirPickerEvent& event) {
		previewCheck(event.GetPath());
	}
	
	void onPreview(wxCommandEvent& event) {
		//Layout();
	}

private :
		wxProgressDialog *dialog;
		MZPosterize::PageSizes sizes;
		MZPosterize posterizer;
		float width;
		float height;
		int counter;
		int max_pages;
	wxDECLARE_EVENT_TABLE();
};

wxBEGIN_EVENT_TABLE(MyFrame, wxFrame)
	EVT_CLOSE(MyFrame::OnCloseWindow)
	EVT_BUTTON(wxID_CANCEL, MyFrame::onCancel)
	EVT_BUTTON(wxID_APPLY, MyFrame::onApply)
	EVT_BUTTON(idMZP_PREVIEW, MyFrame::onPreview)
	EVT_THREAD(wxEVT_COMMAND_MYTHREAD_COMPLETED, MyFrame::onThreadCompletion)
	EVT_THREAD(wxEVT_COMMAND_MYTHREAD_INFO, MyFrame::onThreadUpdate)
	EVT_THREAD(wxEVT_COMMAND_LOG_TXT, MyFrame::onLog)
	EVT_FILEPICKER_CHANGED(idMZP_INPUTFILE, MyFrame::enablePreview)
wxEND_EVENT_TABLE()


MyThread::ExitCode MyThread::Entry() {
	std::cerr << "thread start" << std::endl;
	try {
		posterizer.start([this](int i) { 
				wxThreadEvent* t=new wxThreadEvent(wxEVT_THREAD, wxEVT_COMMAND_MYTHREAD_INFO);
				t->SetInt(i);
				wxQueueEvent(m_handler->GetEventHandler(), t);
			}
		);
		
	} catch (...) {}

	wxQueueEvent(m_handler->GetEventHandler(), new wxThreadEvent(wxEVT_THREAD, wxEVT_COMMAND_MYTHREAD_COMPLETED));
	std::cerr << "thread end" << std::endl;
    return nullptr;
}

class MZPosterizeApp : public wxApp {
	public:
		MZPosterizeApp();
		virtual bool OnInit();
		int OnExit();
	private :
		MyFrame* the_frame;
};

IMPLEMENT_APP(MZPosterizeApp)

MZPosterizeApp::MZPosterizeApp() : wxApp(), the_frame(nullptr) {}

bool MZPosterizeApp::OnInit() {
    the_frame = new MyFrame(nullptr, ID_MYFRAME, argv[0]);
	the_frame->Show(true);
	return true;
}

int MZPosterizeApp::OnExit() {
	wxApp::OnExit();
	return 0;
}

