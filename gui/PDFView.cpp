#include <wx/dcbuffer.h>
#include "PDFView.h"
#include <glib/poppler.h>

wxBEGIN_EVENT_TABLE(PDFView, wxScrolledWindow) EVT_PAINT(PDFView::OnPaint) wxEND_EVENT_TABLE()

    PDFView::PDFView(wxWindow *parent, wxWindowID id, const wxPoint &pos, const wxSize &size, long style)
    : wxScrolledWindow(parent, id, pos, size, style), m_page(NULL) {
}

void PDFView::setPage(PopplerPage *page) {
    m_page = page;
}

void PDFView::OnPaint(wxPaintEvent &event) {

    wxRect rect = GetClientRect();

    if(rect.width == 0 || rect.height == 0 || m_page == NULL) {
		return;
    }

    double dblWidth, dblHeight;
    double dblDpiBy72 = 300 / 72.0;

    poppler_page_get_size(m_page, &dblWidth, &dblHeight);

    int nWidth = (int)(dblWidth * dblDpiBy72);
    int nHeight = (int)(dblHeight * dblDpiBy72);

    cairo_surface_t *st = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, nWidth, nHeight);
    cairo_t *cr = cairo_create(st);

    poppler_page_render(m_page, cr);


    // Allocate a buffer large enough to store the image data. Use malloc()
    // instead of new(). Each
    unsigned int image_buffer_len = rect.width * rect.height * 4;
    unsigned char *image_buffer = (unsigned char *)malloc(image_buffer_len);

    cairo_surface_t *cairo_surface =
        cairo_image_surface_create_for_data(image_buffer, CAIRO_FORMAT_RGB24, rect.width, rect.height, rect.width * 4);
    cairo_t *cairo_image = cairo_create(cairo_surface);

    // Render your object here
	poppler_page_render(m_page, cairo_image);
   // Render(cairo_image, rect.width, rect.height);

    // Now translate the raw image data from the format stored
    // by cairo into a format understood by wxImage.
    unsigned char *output = (unsigned char *)malloc(image_buffer_len);
    int offset = 0;
    for(size_t count = 0; count < image_buffer_len; count += 4) {
	int r = *(image_buffer + count + 2);
	*(output + offset) = r;
	offset++;
	int g = *(image_buffer + count + 1);
	*(output + offset) = g;
	offset++;
	int b = *(image_buffer + count + 0);
	*(output + offset) = b;
	offset++;
    }

    // Create a wxImage from the buffer and then a wxBitmap from that
    wxImage img(rect.width, rect.height, output, true);
    wxBitmap bmp(img);
    wxClientDC client_dc(this);

    // Create a double buffer to draw the plot
    // on screen to prevent flicker from occuring.
    wxBufferedDC dc;
    dc.Init(&client_dc, bmp);

    cairo_destroy(cairo_image);
    cairo_surface_destroy(cairo_surface);
    free(image_buffer);
    free(output);
	
}
