///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version May 28 2015)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "PDFView.h"

#include "mzp_gui.h"

///////////////////////////////////////////////////////////////////////////

mzp_frame::mzp_frame( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxSize( 500,470 ), wxDefaultSize );
	
	m_menubar1 = new wxMenuBar( 0 );
	m_menu1 = new wxMenu();
	m_menubar1->Append( m_menu1, wxT("&File") ); 
	
	m_menu2 = new wxMenu();
	m_menubar1->Append( m_menu2, wxT("&Help") ); 
	
	this->SetMenuBar( m_menubar1 );
	
	wxBoxSizer* bSizer1;
	bSizer1 = new wxBoxSizer( wxHORIZONTAL );
	
	wxBoxSizer* bSizer3;
	bSizer3 = new wxBoxSizer( wxVERTICAL );
	
	wxFlexGridSizer* fgSizer1;
	fgSizer1 = new wxFlexGridSizer( 5, 2, 0, 0 );
	fgSizer1->AddGrowableCol( 1 );
	fgSizer1->SetFlexibleDirection( wxHORIZONTAL );
	fgSizer1->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_staticText1 = new wxStaticText( this, wxID_ANY, wxT("Output Size"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1->Wrap( -1 );
	fgSizer1->Add( m_staticText1, 1, wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	wxArrayString m_sizechoiceChoices;
	m_sizechoice = new wxChoice( this, idMZP_PAGESIZE, wxDefaultPosition, wxDefaultSize, m_sizechoiceChoices, 0 );
	m_sizechoice->SetSelection( 0 );
	fgSizer1->Add( m_sizechoice, 1, wxALL|wxEXPAND, 5 );
	
	m_staticText2 = new wxStaticText( this, wxID_ANY, wxT("Input file"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2->Wrap( -1 );
	fgSizer1->Add( m_staticText2, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	m_inputfile = new wxFilePickerCtrl( this, idMZP_INPUTFILE, wxEmptyString, wxT("Select a file"), wxT("*.*"), wxDefaultPosition, wxDefaultSize, wxFLP_DEFAULT_STYLE|wxFLP_OPEN|wxFLP_USE_TEXTCTRL );
	fgSizer1->Add( m_inputfile, 2, wxALL|wxEXPAND, 5 );
	
	m_staticText3 = new wxStaticText( this, wxID_ANY, wxT("Output file"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText3->Wrap( -1 );
	fgSizer1->Add( m_staticText3, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	m_outputfile = new wxFilePickerCtrl( this, idMZP_OUTPUTFILE, wxEmptyString, wxT("Select a file"), wxT("*.pdf"), wxDefaultPosition, wxDefaultSize, wxFLP_SAVE|wxFLP_USE_TEXTCTRL );
	fgSizer1->Add( m_outputfile, 0, wxALIGN_CENTER_VERTICAL|wxALL|wxEXPAND, 5 );
	
	m_staticText4 = new wxStaticText( this, wxID_ANY, wxT("Add borders"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText4->Wrap( -1 );
	fgSizer1->Add( m_staticText4, 0, wxALIGN_CENTER|wxALL, 5 );
	
	m_chkborders = new wxCheckBox( this, idMZP_BORDERS, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer1->Add( m_chkborders, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	m_staticText5 = new wxStaticText( this, wxID_ANY, wxT("Pagenumber"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText5->Wrap( -1 );
	fgSizer1->Add( m_staticText5, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	m_pagenumber = new wxSpinCtrl( this, wxID_ANY, wxT("1"), wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 1, 9999, 1 );
	fgSizer1->Add( m_pagenumber, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	
	bSizer3->Add( fgSizer1, 0, wxEXPAND, 5 );
	
	m_txtlog = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxHSCROLL|wxTE_MULTILINE|wxTE_READONLY );
	m_txtlog->SetMaxLength( 0 ); 
	m_txtlog->SetMinSize( wxSize( -1,200 ) );
	
	bSizer3->Add( m_txtlog, 2, wxALL|wxEXPAND, 5 );
	
	wxBoxSizer* bSizer2;
	bSizer2 = new wxBoxSizer( wxHORIZONTAL );
	
	m_preview = new wxButton( this, idMZP_PREVIEW, wxT("&Preview"), wxDefaultPosition, wxDefaultSize, 0 );
	m_preview->Enable( false );
	
	bSizer2->Add( m_preview, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	m_apply = new wxStdDialogButtonSizer();
	m_applyApply = new wxButton( this, wxID_APPLY );
	m_apply->AddButton( m_applyApply );
	m_applyCancel = new wxButton( this, wxID_CANCEL );
	m_apply->AddButton( m_applyCancel );
	m_apply->Realize();
	
	bSizer2->Add( m_apply, 1, wxEXPAND, 5 );
	
	
	bSizer3->Add( bSizer2, 0, wxALIGN_BOTTOM|wxALIGN_RIGHT, 5 );
	
	
	bSizer1->Add( bSizer3, 1, wxEXPAND, 5 );
	
	m_inputview = new PDFView( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHSCROLL|wxVSCROLL );
	m_inputview->SetScrollRate( 5, 5 );
	m_inputview->Enable( false );
	m_inputview->Hide();
	
	bSizer1->Add( m_inputview, 1, wxEXPAND | wxALL, 5 );
	
	
	this->SetSizer( bSizer1 );
	this->Layout();
}

mzp_frame::~mzp_frame()
{
}
