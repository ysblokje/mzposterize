#include <wx/wx.h>
#include <wx/window.h>
#include <glib/poppler.h>


class PDFView : public wxScrolledWindow {
public:
	PDFView(
		wxWindow *  	parent,
		wxWindowID  	id = -1,
		const wxPoint &  	pos = wxDefaultPosition,
		const wxSize &  	size = wxDefaultSize,
		long  	style = wxHSCROLL|wxVSCROLL
	);
	void setPage(PopplerPage* page);
private :
	PopplerPage* m_page;
	void OnPaint(wxPaintEvent &);
	wxDECLARE_EVENT_TABLE();
};
