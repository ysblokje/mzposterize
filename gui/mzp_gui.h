///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version May 28 2015)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __MZP_GUI_H__
#define __MZP_GUI_H__

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
class PDFView;

#include <wx/string.h>
#include <wx/menu.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/stattext.h>
#include <wx/choice.h>
#include <wx/filepicker.h>
#include <wx/checkbox.h>
#include <wx/spinctrl.h>
#include <wx/sizer.h>
#include <wx/textctrl.h>
#include <wx/button.h>
#include <wx/scrolwin.h>
#include <wx/frame.h>

///////////////////////////////////////////////////////////////////////////

#define idMZP_PAGESIZE 1000
#define idMZP_INPUTFILE 1001
#define idMZP_OUTPUTFILE 1002
#define idMZP_BORDERS 1003
#define idMZP_PREVIEW 1004

///////////////////////////////////////////////////////////////////////////////
/// Class mzp_frame
///////////////////////////////////////////////////////////////////////////////
class mzp_frame : public wxFrame 
{
	private:
	
	protected:
		wxMenuBar* m_menubar1;
		wxMenu* m_menu1;
		wxMenu* m_menu2;
		wxStaticText* m_staticText1;
		wxChoice* m_sizechoice;
		wxStaticText* m_staticText2;
		wxFilePickerCtrl* m_inputfile;
		wxStaticText* m_staticText3;
		wxFilePickerCtrl* m_outputfile;
		wxStaticText* m_staticText4;
		wxCheckBox* m_chkborders;
		wxStaticText* m_staticText5;
		wxSpinCtrl* m_pagenumber;
		wxTextCtrl* m_txtlog;
		wxButton* m_preview;
		wxStdDialogButtonSizer* m_apply;
		wxButton* m_applyApply;
		wxButton* m_applyCancel;
		PDFView* m_inputview;
	
	public:
		
		mzp_frame( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 501,474 ), long style = wxCAPTION|wxCLOSE_BOX|wxMINIMIZE_BOX|wxRESIZE_BORDER|wxTAB_TRAVERSAL );
		
		~mzp_frame();
	
};

#endif //__MZP_GUI_H__
