"simple" project to posterize a page from a PDF file.

It's not perfect but it fits my needs, maybe more people will enjoy it.


Tools needed to build :
  - cmake <http://www.cmake.org>
  - c++ compiler (tested so far with g++ / clang++)


Libraries used so far :
  - poppler <http://poppler.freedesktop.org/>
  - cairo <http://cairographics.org/>
  - tclap <http://tclap.sourceforge.net/>
  - wxWidgets for the GUI version <http://wxwidgets.org/>

TODOs

  - preview window to get some idea what will be produced
  - dynamic offset 
  - page selection in the gui version
  - get a windows version working, maybe

(C) 2014 Minze Zwerver. MIT licensed.
