#ifndef __MZPOSTERIZE_HPP__
#define __MZPOSTERIZE_HPP__
#include <glib/poppler.h>
#include <cairo/cairo.h>
#include <cairo/cairo-pdf.h>
#include <string>
#include <iostream>
#include <cstring>
#include <cmath>
#include <vector>
#include <cstdlib>
#include <vector>
#include <string>
#include <functional>


// TODO 
// offset x & y as parameter
// list images on page
// allow image selection via parameter

#define POINTS 72.27
#define INCH 25.40

enum errorCodes {
	NO_ERROR=0,
	PARSE_ERROR=1
};

 

class MZPosterize {
	
public :
	typedef std::function<void(int)> page_update_callback;
	typedef std::function<void(std::string)> log_callback;
	
	struct PageSize {
		PageSize() : width(0.0), height(0.0) {}
		PageSize(const std::string &name, float width, float height) :
			name(name), width(width), height(height) {}
		std::string name;
		float width;
		float height;
	};
	typedef std::vector<PageSize> PageSizes;
	
	MZPosterize(log_callback lc=[](const std::string& txt) { std::cout << txt;});
	MZPosterize(const std::string &srcfile, const std::string &destfile="", log_callback lc=[](const std::string& txt) { std::cout << txt;});
	void setLog(log_callback lc);
	void setSource(const std::string &src);
	void setDestinationFile(const std::string &destfile);
	void selectPage(int pagenr);
	void selectImage(int imagenr);
	void setOutputPagesize(float widthmm, float heightmm);
    void setOutputPagesize(const std::string& name);
	void setOutputPageHeight(float heightmm);
	void setOutputPageWidth(float widthmm);
	void changeMarging(float distancemm);
	void init();
	int prepare();
	void start(page_update_callback puc=page_update_callback());
	void convert() { 
		if(prepare())
			start();
	}
	void showReferenceBox(bool show=true);
    PopplerPage* getPage(int page_pointer);
	static PageSizes getSizes();
	
private:
	double mm_to_points(double mm);
	double points_to_mm(double points);
	void copy_portion(cairo_t* cr, cairo_surface_t* src_surface, 
	double page_width, double page_height, double margin, 
		double src_x, double src_y);

	std::string m_file;
	std::string m_dstfile;
	double m_dest_width, m_dest_height;
	double m_margin;
	cairo_surface_t *m_surface;
	cairo_t *m_cr;
	PopplerDocument* m_doc;
	int m_page;
	int m_image;
	double m_output_unit;
	double m_IMAGE_DPI;
	GError* m_error;
	bool m_referencebox;
    PopplerPage *page_pointer;
	long paginas_x;
	long paginas_y;
	double cropped_width;
	double cropped_height;
	double pdf_width, pdf_height;
	double page_pdf_width, page_pdf_height;
	double offset_x, offset_y;
	double pdf_width_mm;
	double pdf_height_mm;
	double margin_all;
	int max_pages;
    PageSizes pagesizes;
	log_callback lc;
};


#endif
