#include "mzposterize.hpp"
#include <sstream>
#include <cmath>

struct Logger {
	Logger(MZPosterize::log_callback lc): lc(lc){}
	std::ostringstream out;
	std::ostringstream& operator<<(const std::string val) {
		out << val;
		return out;
	}
	~Logger() {
		if(lc)
			lc(out.str());
	}
	MZPosterize::log_callback lc;
};

#define LOG  Logger([this](const std::string &txt) { lc(txt);}) 


MZPosterize::MZPosterize(log_callback lc) : lc(lc) { 
	init(); 
}

	void MZPosterize::setLog(log_callback lc) {
		this->lc = lc;
	}

MZPosterize::PageSizes MZPosterize::getSizes() {
    MZPosterize::PageSizes retval;
    retval.push_back(PageSize("ISO A4", 210.0, 290.0));
    retval.push_back(PageSize("ISO A4 Landscape", 290.0, 210.0));
    return retval;
}

MZPosterize::MZPosterize(const std::string &srcfile,
	const std::string &destfile, 
	MZPosterize::log_callback lc) : lc(lc){
    init();
    setSource(srcfile);
    setDestinationFile(destfile);
}

void MZPosterize::setSource(const std::string &src) {
    m_file = src;
    if (!m_doc)
        m_doc = poppler_document_new_from_file(m_file.c_str(), "", &m_error);
}

void MZPosterize::setDestinationFile(const std::string &destfile) { m_dstfile = destfile; }

void MZPosterize::selectPage(int pagenr) { m_page = pagenr; }

void MZPosterize::selectImage(int imagenr) { m_image = imagenr; }

void MZPosterize::setOutputPagesize(float widthmm, float heightmm) {
    m_dest_width = widthmm;
    m_dest_height = heightmm;
}

void MZPosterize::setOutputPagesize(const std::string& name) {
    for(const auto &def : getSizes()) {
        if(def.name == name) {
            m_dest_width = def.width;
            m_dest_height = def.height;
        }
    }
}



void MZPosterize::setOutputPageHeight(float heightmm) { setOutputPagesize(m_dest_width, heightmm); }
void MZPosterize::setOutputPageWidth(float widthmm) { setOutputPagesize(widthmm, m_dest_height); }

void MZPosterize::changeMarging(float distancemm) { m_margin = distancemm; }

double MZPosterize::mm_to_points(double mm) {
    double retval = mm / INCH;
    retval *= POINTS;
    return (retval);
}

double MZPosterize::points_to_mm(double points) {
    double retval = points / POINTS;
    retval *= INCH;
    return (retval);
}

void MZPosterize::showReferenceBox(bool show) { m_referencebox = show; }

void MZPosterize::init() {
    m_page = 0;
    m_image = -1;
    m_doc = NULL;
    m_cr = NULL;
    m_surface = NULL;
    m_margin = 10.0;
    m_dest_width = 210.0;
    m_dest_height = 290.0;
    m_output_unit = INCH / POINTS;
    m_IMAGE_DPI = 300.0;
    m_error = NULL;
    m_referencebox = true;
}

void MZPosterize::copy_portion(cairo_t *cr,
                               cairo_surface_t *src_surface,
                               double page_width,
                               double page_height,
                               double margin,
                               double src_x,
                               double src_y) {
    cairo_save(cr);
    cairo_set_operator(cr, CAIRO_OPERATOR_CLEAR);
    //cairo_set_source_rgb(cr, 255, 255, 255);
    cairo_paint(cr);
    cairo_restore(cr);

    auto tmp_x = mm_to_points(src_x + margin);
    auto tmp_y = mm_to_points(src_y + margin);
    cairo_set_source_surface(cr, src_surface, tmp_x , tmp_y);
    cairo_rectangle(cr,
                    mm_to_points(margin),
                    mm_to_points(margin),
                    mm_to_points(page_width - margin * 2),
                    mm_to_points(page_height - margin * 2));
    cairo_fill(cr);

    if (m_referencebox) {
        cairo_save(cr);
        cairo_set_operator(cr, CAIRO_OPERATOR_ATOP);

        cairo_set_source_rgb(cr, 1, 0, 0);
        cairo_set_line_width(cr, 1);
        cairo_rectangle(cr,
                        mm_to_points(margin),
                        mm_to_points(margin),
                        mm_to_points(page_width - margin * 2),
                        mm_to_points(page_height - margin * 2));
        cairo_stroke(cr);


    #define DEG(A) A *(M_PI / 180.0)

        cairo_set_source_rgb(cr, 1, 0, 0);
        cairo_set_line_width(cr, 1);
        cairo_arc(cr, mm_to_points(margin), mm_to_points(margin), mm_to_points(10.0), DEG(0.0), DEG(90.0));
        cairo_stroke(cr);
        cairo_arc(cr, mm_to_points(page_width - margin), mm_to_points(margin), mm_to_points(10.0), DEG(90), DEG(180));
        cairo_stroke(cr);

        cairo_arc(cr,
                  mm_to_points(page_width - margin),
                  mm_to_points(page_height - (margin)),
                  mm_to_points(10.0),
                  DEG(180),
                  DEG(270));
        cairo_stroke(cr);

        cairo_arc(cr, mm_to_points(margin), mm_to_points(page_height - (margin)), mm_to_points(10.0), DEG(270), DEG(0));
        cairo_stroke(cr);

        cairo_restore(cr);
    }
}

PopplerPage* MZPosterize::getPage(int page) {
    PopplerPage *p{};
	if(m_doc)
		p = poppler_document_get_page(m_doc, page);
	return p;
}

int MZPosterize::prepare() {
    page_pointer = getPage(m_page);
    if (page_pointer) {
	cropped_width = m_dest_width - 2 * m_margin;
	cropped_height = m_dest_height - 2 * m_margin;

	offset_x = (0);
	offset_y = (0);

    poppler_page_get_size(page_pointer, &page_pdf_width, &page_pdf_height);

    GList *image_list{};
    gint id{-1};

    pdf_width = page_pdf_width;
    pdf_height = page_pdf_height;

    if (m_image >= 0) {
        image_list = poppler_page_get_image_mapping(page_pointer);
	    GList *first(g_list_first(image_list));
	    if (first) {
            PopplerRectangle rect = *static_cast<PopplerRectangle *>(first->data);
            id = static_cast<PopplerImageMapping *>(first->data)->image_id;
            pdf_width = rect.x2 - rect.x1;
            pdf_height = rect.y2 - rect.y1;
            offset_x = points_to_mm(rect.x1);
            offset_y = points_to_mm(rect.y1);
	    }
    }
	pdf_width_mm = points_to_mm(pdf_width);
	pdf_height_mm = points_to_mm(pdf_height);
	LOG << "Width : " << pdf_width_mm << "mm\n";
	LOG << "      : " << pdf_width << "pts\n";
	LOG << "Height: " << pdf_height_mm << "mm\n";
	LOG << "      : " << pdf_height << "pts\n";

	// margin van 20mm
	margin_all = m_margin / m_output_unit;

	LOG << "margin : " << margin_all << "pts\n";
    LOG << "page width : " << m_dest_width << "mm, pts " << mm_to_points(m_dest_width) << "\n";
    LOG << "page height : " << m_dest_height << "mm, pts " << mm_to_points(m_dest_height) << "\n";
	LOG << "cropped width : " << m_dest_width - m_margin * 2 << "mm, pts "
	          << (m_dest_width - m_margin) / m_output_unit << "\n";
	LOG << "cropped height : " << m_dest_height - m_margin * 2 << "mm, pts "
	          << (m_dest_height - m_margin) / m_output_unit << "\n";
	paginas_x = ceil(pdf_width / mm_to_points(cropped_width));
	paginas_y = ceil(pdf_height / mm_to_points(cropped_height));

	LOG << "No of pages : " << paginas_x << " x " << paginas_y << "\n";
	return paginas_x * paginas_y;
    }
    return 0;
}

void MZPosterize::start(page_update_callback puc) {
    if (!m_doc && m_error) {
	std::cerr << m_error->message << std::endl;
    } else {
    page_pointer = getPage(m_page);
    if (page_pointer) {

	    // full page
	    cairo_surface_t *src_surface =
            cairo_image_surface_create(CAIRO_FORMAT_ARGB32, page_pdf_width, page_pdf_height);
	    cairo_t *src_ctx = cairo_create(src_surface);
        cairo_save(src_ctx);
        poppler_page_render(page_pointer, src_ctx);
	    cairo_restore(src_ctx);
	    cairo_set_operator(src_ctx, CAIRO_OPERATOR_DEST_OVER);
        cairo_set_source_rgb(src_ctx, 1., 1., 1.);
	    cairo_paint(src_ctx);

	    m_surface =
	        cairo_pdf_surface_create(m_dstfile.c_str(), mm_to_points(m_dest_width), mm_to_points(m_dest_height));
	    cairo_t *dst_ctx = cairo_create(m_surface);
		int page(0);
	    for (int y = 0; y < paginas_y; y++) {
		for (int x = 0; x < paginas_x; x++) {
			if(puc)
				puc(page);
		    cairo_save(dst_ctx);

		    double width = m_dest_width;
		    double height = m_dest_height;

		    if (x * cropped_width + cropped_width > pdf_width_mm)
            width = pdf_width_mm - (x * cropped_width) + 2*m_margin + offset_x;
		    if (y * cropped_height + cropped_height > pdf_height_mm)
            height = pdf_height_mm - (y * cropped_height) + 2*m_margin + offset_y;
		    copy_portion(dst_ctx,
		                 src_surface,
		                 width,
		                 height,
                         m_margin,
                         2 * offset_x - (x * cropped_width),
                         2 * offset_y - (y * cropped_height));
		    cairo_set_operator(dst_ctx, CAIRO_OPERATOR_DEST_OVER);
		    cairo_set_source_rgb(dst_ctx, 1, 1, 1);
		    cairo_paint(dst_ctx);
		    cairo_restore(dst_ctx);

		    cairo_show_page(dst_ctx);
		    LOG << "*";
		    
			page++;
		}
		LOG << "\n";
	    }
	    LOG << "\n";

	    cairo_surface_destroy(m_surface);
	    cairo_destroy(dst_ctx);

	} else {
	    LOG << "Page does not exist." << std::endl;
	}
	g_object_unref(m_doc);
    }
}
